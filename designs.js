// Select color input
// Select size input

// When size is submitted by the user, call makeGrid()

function makeGrid() {

// Your code goes here!
    const height = $('#inputHeight').val();
    const width = $('#inputWeight').val();

    for (let i=1; i<=height; i++) {
        const row = document.createElement("tr");

        for (let j=1; j<=width; j++) {
            $(row).append("<td onclick='onColumnClick(this)'></td>");
        }

        $('#pixelCanvas').append(row);
    }
}

function onColumnClick(val) {
    const colorPicker = $('#colorPicker').val();
    $(val).css('background-color', colorPicker);
}

$(document).ready(function() {
    $('#submit').click(function(e) {
        e.preventDefault();
        makeGrid();
    });
});
